package it.sauronsoftware.jave;

/*
 *  *****************************************************************************
 *                    Lean Survey Pesquisas de Mercado S.A
 *  -----------------------------------------------------------------------------
                          http://www.leansurvey.com.br
 *                      Contato: <dev@leansurvey.com.br>
 *  *****************************************************************************
 */
public class MyFFMPEGLocator extends FFMPEGLocator {

    @Override
    protected String getFFMPEGExecutablePath() {
        return "/usr/local/bin/ffmpeg";
    }

}
